/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Tuf Gaming
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       // frame.setSize(new Dimension(500,300));
        frame.setSize(500, 300);//Overloading method
        
        JLabel lbHelloWold = new JLabel("Hello Wold!!",JLabel.CENTER);
       
        lbHelloWold.setBackground(Color.ORANGE);
        lbHelloWold.setOpaque(true);
        lbHelloWold.setFont(new Font("Verdana", Font.PLAIN, 25));
         frame.add(lbHelloWold);
        frame.setVisible(true);
    }
 
}
